#!/usr/bin/env python3

import numpy as np
import cv2
import pyyolo
import asyncio
import threading
import boto3
from concurrent.futures import ThreadPoolExecutor

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

cap = cv2.VideoCapture('test.mp4')

meta = "mdata/darknet.data"
cfg = "mdata/darknet-yolov3.cfg"
weights = "mdata/darknet-yolov3.weights"

thresh = 0.45
hier_thresh = 0.5

pyyolo.init(".", meta, cfg, weights)

executor = ThreadPoolExecutor(max_workers=10)

def save_s3(img):
    s3.put_object(Bucket='detected', Key='object_name', Body='TEST')
    print("object has been put in the storage")

def send_event(url):
    print ("Event has been sent")

def process_detections(detections, img):
    save_s3(img)
    send_event("")

def main():
    cap = cv2.VideoCapture("test.mp4")
    while True:
        flag, img_orig = cap.read()
        
        img = img_orig.transpose(2,0,1)
        c, h, w = img.shape[0], img.shape[1], img.shape[2]
        data = img.ravel()/255.0
        data = np.ascontiguousarray(data, dtype=np.float32)
        detections = pyyolo.detect(w, h, c, data, thresh, hier_thresh)	
        
        executor.submit(process_detections, detections, img_orig)
        if cv2.waitKey(1) == ord('q'):
            break
    
    pyyolo.cleanup()

main()
print("Finished")
